/// A tile of the map and it's properties
#[derive(Clone, Copy, Debug)]
pub(crate) struct Tile {
    pub(crate) blocked: bool,
    pub(crate) explored: bool,
    pub(crate) block_sight: bool,
}

impl Tile {
    pub fn empty() -> Self {
        Tile {
            blocked: false,
            explored: false,
            block_sight: false,
        }
    }

    pub fn wall() -> Self {
        Tile {
            blocked: true,
            explored: false,
            block_sight: true,
        }
    }
}