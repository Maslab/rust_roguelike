use tcod::{BackgroundFlag, Color, Console};

// This is a generic object: the player, a monster, an item, the stairs...
// It's always represented by a character on screen.
#[derive(Debug)]
pub(crate) struct Object {
    pub(crate) x: i32,
    pub(crate) y: i32,
    character: char,
    color: Color,
    pub(crate) name: String,
    pub(crate) blocks: bool,
    pub(crate) alive: bool,
}

impl Object {
    pub fn new(x: i32, y: i32, character: char, name: &str, color: Color, blocks: bool) -> Self {
        Object { x, y, character, color, name: name.into(), blocks, alive: false }
    }

    /// Set the color and then draw the character that represents this object at its position
    pub fn draw(&self, con: &mut dyn Console) {
        con.set_default_foreground(self.color);
        // Draw character '@' at coordinates
        // BackgroundFlag::None says to ignore the default background color
        con.put_char(self.x, self.y, self.character, BackgroundFlag::None);
    }

    pub fn pos(&self) -> (i32, i32) {
        (self.x, self.y)
    }

    pub fn set_pos(&mut self, x: i32, y: i32) {
        self.x = x;
        self.y = y;
    }
}